﻿using System;

namespace ConsoleApp3
{
    class Program
    {

        static void Main(string[] args)
        {

            Car1 car = new Car1(true);



        }
    }
    abstract class Car
    {
        abstract protected bool isgoing { get; set; }
        protected virtual int maxxspeed { get; set; }
        protected virtual int maxxpeople { get; set; }
        protected virtual int weight { get; set; }

        public virtual void Print()
        {
            Console.WriteLine($"weight: {weight}");
        }
        protected virtual void IncreaseSpeed()
        {
            int a = maxxspeed;
            maxxspeed = a * 2;

        }
        protected virtual void Increaseweight()
        {
            int c = maxxpeople;
            weight += c * 2;

        }
        protected virtual void Icreasepeople()
        {
            if (maxxpeople < 5)
            {
                maxxpeople++;
            }
            else
            {
                Console.WriteLine("outofrange");
            }

        }
        abstract protected void Start();

        abstract protected void Stop();

        abstract protected void Crash();

        protected Car ()
        {

        }

        public Car(bool Isgoing)
        {
            isgoing = Isgoing;
            maxxpeople = 3;
            maxxspeed = 3;
            weight = 3;
        }
    }
    class Car1 : Car
    {
        public Car1(bool isgoing) :base (isgoing)
        {
            
        }
        protected override void Start()
        {
            Console.WriteLine("9 ho4u pitsu");
        }
        protected override void Stop()
        {
            Console.WriteLine("9 ho4u pitsu");
        }
        protected override void Crash()
        {
            Console.WriteLine("9 ho4u pitsu");
        }

        protected override bool isgoing { get; set; }
        

        public override void Print ()
        {
            base.Print();
            Console.WriteLine($"maxxpeople: {maxxpeople}  maxxspeed: {maxxspeed}");
        }

        
    }

    class Car2 : Car

    {
        protected override void Start()
        {
            Console.WriteLine("9 ho4u pitsu2");
        }
        protected override void Stop()
        {
            Console.WriteLine("9 ho4u pitsu2");
        }
        protected override void Crash()
        {
            Console.WriteLine("9 ho4u pitsu2");
        }

        protected override bool isgoing
        {
            get { return isgoing; }
            set { isgoing = value; }

        }

        protected override void Icreasepeople()
        {
            base.Icreasepeople();
            if (maxxpeople < 4)
            {
                maxxpeople++;
            }
            else
            {
                Console.WriteLine("outofrange");
            }
        }

    }

    

    class Car3 : Car
    {
        protected override void Start()
        {
            Console.WriteLine("9 ho4u pitsu2");
        }
        protected override void Stop()
        {
            Console.WriteLine("9 ho4u pitsu2");
        }
        protected override void Crash()
        {
            Console.WriteLine("9 ho4u pitsu2");
        }

        protected override bool isgoing
        {
            get { return isgoing; }
            set { isgoing = value; }

        }
        protected override void IncreaseSpeed()
        {
            base.IncreaseSpeed();
            int a = maxxspeed;
            maxxspeed = a * 3;
        }
    }


   
}
