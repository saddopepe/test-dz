﻿using System;

namespace konvertor
{

   public enum currency
    {
        Usd,
        Euro,
        Rub
    }

    class Convertor
    {
        private static int count;
        private double usd;
        private double rub;
        private double euro;
        public Convertor (double Usd, double Rub, double Euro)
        {
            usd = Usd;
            rub = Rub;
            euro = Euro;
        }


        public double Convert(currency value, double uah )
        {
            double b;

            if (currency.Usd == value)
            {
                b = uah * usd;
            }
            else if(currency.Euro == value)
            {
                b = uah * euro;
            }
            else
            {
                b = uah * rub;
            }
            count++;
            return b;
        }


        public static int OveralCountOfConvertion()
        {
            return count;
        }
        public void Printname ()
        {
            Console.WriteLine($"Usd: {usd}\n" +
                $"Rub: {rub}\n" +
                $"Euro {euro}\n");
                
        }
          
    }



    class Program
    {
        static void Main(string[] args)
        {
            
            Convertor convertor = new Convertor(0.035, 0.030, 2.66);

            Convertor convertor1 = new Convertor(0.035, 0.030, 2.66);

            

            convertor.Printname();

            convertor.Convert(currency.Usd, 50);
            convertor.Convert(currency.Usd, 50);

            convertor1.Convert(currency.Usd,50);
            convertor1.Convert(currency.Usd, 50);

            Console.WriteLine(Convertor.OveralCountOfConvertion());

            Console.WriteLine(convertor.Convert(currency.Usd,50));
        }
    }
}
